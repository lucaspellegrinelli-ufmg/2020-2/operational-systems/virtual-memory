#ifndef PAGINA_H
#define PAGINA_H

typedef struct {
  unsigned id_pagina;
  int sujo;
  long tempo_adicionada;
} pagina;

typedef struct {
  pagina pagina;
  long ultimo_acesso;
} quadro;

#endif