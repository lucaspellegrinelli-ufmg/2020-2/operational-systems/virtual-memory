#ifndef ALGORITMOS_H
#define ALGORITMOS_H

#include <stdio.h>
#include <stdlib.h>

#include "pagina.h"
#include "contador.h"

void lru(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador);
void fifo(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador);
void segunda_chance(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador);
void aleatorio(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador);

#endif