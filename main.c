#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#include "pagina.h"
#include "algoritmos.h"
#include "contador.h"

void imprimir_quadros(quadro* quadros, int n_quadros) {
  printf("%-11s | %-12s | %-6s | %-10s\n", "endereço", "pagina", "suja", "bit de ref."); 
  for (int i = 0; i < n_quadros; i++) {
    int ref_bit = quadros[i].pagina.tempo_adicionada != quadros[i].ultimo_acesso;
    printf("%-10d | %-12d | %-6d | %-10d\n", i, quadros[i].pagina.id_pagina, quadros[i].pagina.sujo, ref_bit); 
  }
}

unsigned calcular_pagina(int tamanho_pagina, unsigned addr) {
  unsigned tmp = tamanho_pagina;
  unsigned s = 0;
  while (tmp > 1) {
    tmp = tmp >> 1;
    s++;
  }

  return addr >> s;
}

int main(int argc, char **argv){
  struct timeval start, end;
  if(argc < 5){
    printf("Parâmetros incorretos. Execute o programa como\n");
    printf("   ./tp2virtual [algoritmo] [arquivo com a sequência de endereços] [tamanho das páginas (em KB)] [tamanho total (em KB)] \n");
    exit(0);
  }

  srand(time(NULL));

  long tempo = 0;

  char *algortimo = argv[1];
  char *nome_arquivo = argv[2];
  int tamanho_pagina = atoi(argv[3]) * 1024;
  int tamanho_total = atoi(argv[4]) * 1024;
  int n_quadros = tamanho_total / tamanho_pagina;

  contador_t *contador = (contador_t*) malloc (sizeof(contador_t));
  contador->acessos_mem = 0;
  contador->page_faults = 0;
  contador->paginas_sujas = 0;
  contador->fila_quadros = 0;

  quadro* quadros = (quadro*) malloc (sizeof(quadro) * n_quadros);
  for(int i = 0; i < n_quadros; i++){
    pagina pagina_vazia = { -1, 0, -1 };
    quadros[i].pagina = pagina_vazia;
    quadros[i].ultimo_acesso = -1;
  }

  unsigned addr;
  char rw;
  FILE *arquivo = fopen(nome_arquivo, "r");

  printf("Tabela de quadros antes da execução do algoritmo\n");
  imprimir_quadros(quadros, n_quadros);

  gettimeofday(&start, NULL);

  while(fscanf(arquivo,"%x %c", &addr, &rw) != EOF){
    unsigned pagina = calcular_pagina(tamanho_pagina, addr);

    if(strcmp(algortimo, "lru") == 0)
      lru(quadros, n_quadros, pagina, rw, &tempo, contador);
    else if(strcmp(algortimo, "fifo") == 0)
      fifo(quadros, n_quadros, pagina, rw, &tempo, contador);
    else if(strcmp(algortimo, "2a") == 0)
      segunda_chance(quadros, n_quadros, pagina, rw, &tempo, contador);
    else
      aleatorio(quadros, n_quadros, pagina, rw, &tempo, contador);
  }

  printf("\nTabela de quadros após a execução do algoritmo\n");
  imprimir_quadros(quadros, n_quadros);

  gettimeofday(&end, NULL);

  double time_taken = end.tv_sec + end.tv_usec / 1e6 -
                        start.tv_sec - start.tv_usec / 1e6;

  fclose(arquivo);
  printf("\nEstatísticas da execução\n");
  printf("Arquivo de entrada: %s\n", nome_arquivo);
  printf("Tamanho da memoria: %dKB\n", tamanho_total / 1024);
  printf("Tamanho das páginas: %dKB\n", tamanho_pagina / 1024);
  printf("Tecnica de reposicao: %s\n", algortimo);
  printf("Número de acessos na memória %d\n", contador->acessos_mem);
  printf("Número de page faults %d\n", contador->page_faults);
  printf("Número de paginas sujas %d\n", contador->paginas_sujas);
  printf("Tempo de execução %f\n", time_taken);
}