#ifndef CONTADOR_H
#define CONTADOR_H

typedef struct {
  int acessos_mem;
  int page_faults;
  int paginas_sujas;
  int fila_quadros;
} contador_t;

#endif