import os
import numpy as np
import matplotlib.pyplot as plt

cmd = "./tp2virtual lru compilador/compilador.log 4 128"
stream = os.popen(cmd)
output = stream.read()
print()

all_times = np.zeros(5)
for iv, v in enumerate([1, 2, 4, 8, 16]):
  cmd = "./tp2virtual lru compilador/compilador.log " + str(v) + " 128"
  stream = os.popen(cmd)
  output = stream.read()
  all_times[iv] = float(output.split(" ")[-8].split("\n")[0])

plt.title("Variação do tamanho das páginas em [1KB, 2KB, 4KB, 8KB, 16KB]")
plt.plot(all_times, label="Número de page faults")
plt.legend(loc="best")
plt.ylabel("Número de page faults")
plt.xlabel("Tamanho da página (KB)")
plt.xticks([0, 1, 2, 3, 4], [1, 2, 4, 8, 16])
plt.show()