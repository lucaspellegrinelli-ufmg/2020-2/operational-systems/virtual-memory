#include "algoritmos.h"
#include "pagina.h"
#include "contador.h"

void lru(quadro *quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador){
  if (operacao == 'R' || operacao == 'W')
    contador->acessos_mem++;
  
  int achou_pagina = 0;
  for(int i = 0; i < n_quadros; i++){
    if(quadros[i].pagina.id_pagina == pagina) {
      achou_pagina = 1;
      quadros[i].ultimo_acesso = *tempo;
      quadros[i].pagina.sujo = quadros[i].pagina.sujo || operacao == 'W';
      (*tempo)++;
      break;
    }
  }

  if(achou_pagina == 0) {
    contador->page_faults++;

    int indice_quadro = -1;
    int quadro_ultimo_acesso = 999999999;
    for(int i = 0; i < n_quadros; i++){
      if(quadros[i].pagina.id_pagina == -1){
        indice_quadro = i;
        break;
      }

      if(quadros[i].ultimo_acesso < quadro_ultimo_acesso){
        indice_quadro = i;
        quadro_ultimo_acesso = quadros[i].ultimo_acesso;
      }
    }

    if (quadros[indice_quadro].pagina.sujo == 1) {
      contador->paginas_sujas++;
    }

    quadros[indice_quadro].pagina.id_pagina = pagina;
    quadros[indice_quadro].pagina.sujo = operacao == 'W';
    quadros[indice_quadro].ultimo_acesso = *tempo;
    (*tempo)++;
  }
}

void fifo(quadro *quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador){
  if (operacao == 'R' || operacao == 'W')
    contador->acessos_mem++;
  
  int achou_pagina = 0;
  for(int i = 0; i < n_quadros; i++){
    if(quadros[i].pagina.id_pagina == pagina) {
      achou_pagina = 1;
      quadros[i].pagina.sujo = quadros[i].pagina.sujo || operacao == 'W';
      break;
    }
  }

  if(achou_pagina == 0) {
    contador->page_faults++;

    int indice_quadro = -1;
    int pag_tempo_adicionada = 999999999;
    for(int i = 0; i < n_quadros; i++){
      if(quadros[i].pagina.id_pagina == -1){
        indice_quadro = i;
        break;
      }

      if(quadros[i].pagina.tempo_adicionada < pag_tempo_adicionada){
        indice_quadro = i;
        pag_tempo_adicionada = quadros[i].pagina.tempo_adicionada;
      }
    }

    if (quadros[indice_quadro].pagina.sujo == 1) {
      contador->paginas_sujas++;
    }

    quadros[indice_quadro].pagina.id_pagina = pagina;
    quadros[indice_quadro].pagina.sujo = operacao == 'W';
    quadros[indice_quadro].pagina.tempo_adicionada = *tempo;
    (*tempo)++;
  }
}

void segunda_chance(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador){
  if (operacao == 'R' || operacao == 'W')
    contador->acessos_mem++;
  
  int achou_pagina = 0;
  for(int i = 0; i < n_quadros; i++){
    if(quadros[i].pagina.id_pagina == pagina) {
      achou_pagina = 1;
      quadros[i].ultimo_acesso = *tempo;
      quadros[i].pagina.sujo = quadros[i].pagina.sujo || operacao == 'W';
      (*tempo)++;
      break;
    }
  }

  if(achou_pagina == 0) {
    contador->page_faults++;

    int indice_quadro = -1;
    while (1) {
      int ref_bit = quadros[contador->fila_quadros].pagina.tempo_adicionada != quadros[contador->fila_quadros].ultimo_acesso;

      if (ref_bit == 0) {
        indice_quadro = contador->fila_quadros;
        contador->fila_quadros = (contador->fila_quadros + 1) % n_quadros;
        break;
      }

      quadros[contador->fila_quadros].ultimo_acesso = quadros[contador->fila_quadros].pagina.tempo_adicionada;
      contador->fila_quadros = (contador->fila_quadros + 1) % n_quadros;
    }

    if (quadros[indice_quadro].pagina.sujo == 1) {
      contador->paginas_sujas++;
    }

    quadros[indice_quadro].pagina.id_pagina = pagina;
    quadros[indice_quadro].pagina.sujo = operacao == 'W';
    quadros[indice_quadro].pagina.tempo_adicionada = *tempo;
    quadros[indice_quadro].ultimo_acesso = *tempo;
    (*tempo)++;
  }
}

void aleatorio(quadro* quadros, int n_quadros, int pagina, char operacao, long *tempo, contador_t *contador) {
  if (operacao == 'R' || operacao == 'W')
    contador->acessos_mem++;
  
  int achou_pagina = 0;
  for(int i = 0; i < n_quadros; i++){
    if(quadros[i].pagina.id_pagina == pagina) {
      achou_pagina = 1;
      quadros[i].pagina.sujo = quadros[i].pagina.sujo || operacao == 'W';
      break;
    }
  }

  if(achou_pagina == 0) {
    contador->page_faults++;

    int indice_quadro = rand() % n_quadros;
    if (quadros[indice_quadro].pagina.sujo == 1) {
      contador->paginas_sujas++;
    }

    quadros[indice_quadro].pagina.id_pagina = pagina;
    quadros[indice_quadro].pagina.sujo = operacao == 'W';
    quadros[indice_quadro].pagina.tempo_adicionada = *tempo;
    (*tempo)++;
  }
}